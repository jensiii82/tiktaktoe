let player1 = "first"
let player2 = "second"
let player1Name = "Spieler 1"
let player2Name = "Spieler 2"
let currentPlayer = player1
function onClicked(field) {
  field.player = currentPlayer
  field.disabled = true
  if(currentPlayer==player1) {
    field.classList.add('first')
    currentPlayer = player2
  } else {
    field.classList.add('second')
    currentPlayer = player1
  }
  checkWinner()
}

function checkWinner() {
  var table = document.getElementById("tiktaktoe").children[0]
	var fields = []
	for (let index1 = 0; index1 < table.children.length; index1++) {
		const row = table.children[index1];
		fields.push(new Array())
		for (let index2 = 0; index2 < row.children.length; index2++) {
			const field = row.children[index2].children[0].player;
			fields[index1].push(field)
		}
	}
  let winner = checkRows(fields)
  if (winner) {
  } else {
    winner = checkRows(switchArray(fields))
    if (winner){
    } else {
      winner = checkDiagonals(fields)
    }
  }
  if(winner == 'first') {
    document.getElementById("winner").value = `${player1Name} hat gewonnen`
  } else if(winner == 'second') {
    document.getElementById("winner"). value = `${player2Name} hat gewonnen`
  }
}

function checkRows(fields) {
  for (let index = 0; index < fields.length; index++) {
    const element = fields[index];
    if(element[0] == element[1] && element[1] == element[2] && element[0]) {
      return element[0]
    }
  }
  return false
}
function switchArray(fields) {
  var switchedArray = [];
  for(var i = 0; i < fields.length; i++){
      switchedArray.push([]);
  };

  for(var i = 0; i < fields.length; i++){
      for(var j = 0; j < fields.length; j++){
          switchedArray[j].push(fields[i][j]);
      };
  };

  return switchedArray;
}
function checkDiagonals(fields) {
  if (fields[0][0] == fields[1][1] && fields[1][1] == fields[2][2]) {
    return fields[0][0]
  } else if (fields[0][2] == fields[1][1] && fields[1][1] == fields[2][0]) {
    return fields[0][2]
  }
  return false
}
function changePlayer1Name(input) {
  player1Name = input.value
}
function changePlayer2Name(input) {
  player2Name = input.value
}